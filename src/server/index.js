let topTenPlayers = require("./top-ten-players.cjs");
let matchesPerCity = require("./matches-played-per-city.cjs");
let matchesWonByTeam = require("./matches-won-by-team.cjs");
let redCardByTeam = require("./red-card-per-team-2014worldCup.cjs");

const csvPlayers =
  "/home/pranam/Desktop/Javascript-Assingments/fifa-project/src/data/WorldCupPlayers.csv";

const csvFilePath =
  "/home/pranam/Desktop/Javascript-Assingments/fifa-project/src/data/WorldCupMatches.csv";

const csv = require("csvtojson");
const fs = require("fs");

csv()
  .fromFile(csvPlayers)
  .then((players) => {
    let value = topTenPlayers(players);
    let value1 = JSON.stringify(value);
    fs.writeFile(
      "/home/pranam/Desktop/Javascript-Assingments/fifa-project/src/public/output/top-ten-players.json",
      value1,
      (error) => console.error(error)
    );
  });

csv()
  .fromFile(csvFilePath)
  .then((matches) => {
    let result = JSON.stringify(matchesPerCity(matches));
    fs.writeFile(
      "/home/pranam/Desktop/Javascript-Assingments/fifa-project/src/public/output/matches-played-per-city.json",
      result,
      (error) => console.error(error)
    );
  });

csv()
  .fromFile(csvFilePath)
  .then((matches) => {
    let result1 = JSON.stringify(matchesWonByTeam(matches));
    fs.writeFile(
      "/home/pranam/Desktop/Javascript-Assingments/fifa-project/src/public/output/matches-won-by-team.json",
      result1,
      (error) => console.error(error)
    );
  });

csv()
  .fromFile(csvFilePath)
  .then((matches) =>
    csv()
      .fromFile(csvPlayers)
      .then((players) => {
        const answer = JSON.stringify(redCardByTeam(matches, players,2010));
        fs.writeFile(
          "/home/pranam/Desktop/Javascript-Assingments/fifa-project/src/public/output/red-card-per-team-2014worldCup.json",
          answer,
          (error) =>
            error
              ? console.error("Error writing JSON file:", error)
              : console.log("JSON file written successfully.")
        );
      })
      .catch((error) => console.error("Error reading players CSV:", error))
  )
  .catch((error) => console.error("Error reading matches CSV:", error));
