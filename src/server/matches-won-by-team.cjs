function getMatchesWonByTeams(matches) {
  const matchesWonByTeam = matches.reduce((accumulator, match) => {
    let homeTeamGoals = match["Home Team Goals"];
    let awayTeamGoals = match["Away Team Goals"];
    let awayTeamName = match["Away Team Name"];
    let homeTeamName = match["Home Team Name"];

    if (homeTeamGoals > awayTeamGoals) {
      if (accumulator[homeTeamName]) {
        accumulator[homeTeamName]++;
      } else {
        accumulator[homeTeamName] = 1;
      }
    }
    if (homeTeamGoals < awayTeamGoals) {
      if (accumulator[awayTeamName]) {
        accumulator[awayTeamName]++;
      } else {
        accumulator[awayTeamName] = 1;
      }
    }

    return accumulator;
  }, {});

  return matchesWonByTeam;
}

module.exports = getMatchesWonByTeams;
