function matchesPerCity(matches) {
  let matchPlayedAllCities = matches.reduce((accumulator, match) => {
    let city = match["City"];
    if (accumulator[city]) {
      accumulator[city]++;
    } else {
      accumulator[city] = 1;
    }
    return accumulator;
  }, {});
 return matchPlayedAllCities;
}

module.exports = matchesPerCity;
